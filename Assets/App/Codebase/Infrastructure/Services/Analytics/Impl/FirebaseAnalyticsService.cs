using App.Codebase.Infrastructure.Services.Analytics.Adapter;
using Firebase;
using Firebase.Analytics;
using Firebase.Extensions;
using UnityEngine;
using VContainer.Unity;

namespace App.Codebase.Infrastructure.Services.Analytics.Impl
{
    public class FirebaseAnalyticsService : IAnalyticsAdapter, IStartable
    {
        public bool FirebaseInitialized { get; private set; }
        private DependencyStatus _dependencyStatus = DependencyStatus.UnavailableOther;


        public void Start()
        {
            if (FirebaseInitialized)
                return;
            
            ResolveDependencies();
        }

        void IAnalyticsAdapter.TrackEvent(string eventName, string parameter, object value)
        {
            switch (value)
            {
                case double doubleValue:
                    FirebaseAnalytics.LogEvent(eventName, new Parameter(parameter, doubleValue));
                    break;
                case long longValue:
                    FirebaseAnalytics.LogEvent(eventName, new Parameter(parameter, longValue));
                    break;
                case string stringValue:
                    FirebaseAnalytics.LogEvent(eventName, new Parameter(parameter, stringValue));
                    break;
                default:
                    Debug.LogError("Invalid value type. Supported types are double, long, and string.");
                    break;
            }
        }

        private void ResolveDependencies()
        {
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
            {
                _dependencyStatus = task.Result;

                switch (_dependencyStatus)
                {
                    case DependencyStatus.Available:
                        InitializeFirebase();
                        break;
                    default:
                        Debug.LogError("Could not resolve all Firebase dependencies: " + _dependencyStatus);
                        break;
                }
            });
        }

        private void InitializeFirebase() 
        {
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
            FirebaseInitialized = true;
        }
    }
}
