using App.Codebase.Infrastructure.Services.Analytics.Adapter;
using VContainer;

namespace App.Codebase.Infrastructure.Services.Analytics
{
    public sealed class AnalyticsService : IAnalyticsService
    { 
        private readonly IAnalyticsAdapter _analyticsAdapter;

        [Inject]
        public AnalyticsService(IAnalyticsAdapter analyticsAdapter)
        {
            _analyticsAdapter = analyticsAdapter;
        }

        void IAnalyticsService.TrackEvent(string eventName, string parameter, object value)
        {
            _analyticsAdapter?.TrackEvent(eventName, parameter, value);
        }
    }
}
