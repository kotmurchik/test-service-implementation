namespace App.Codebase.Infrastructure.Services.Analytics.Adapter
{
    public interface IAnalyticsAdapter
    { 
        void TrackEvent(string eventName, string parameter, object value);
    }
}
