namespace App.Codebase.Infrastructure.Services.Analytics
{
    public interface IAnalyticsService
    {
        void TrackEvent(string eventName, string parameter, object value);
    }
}
