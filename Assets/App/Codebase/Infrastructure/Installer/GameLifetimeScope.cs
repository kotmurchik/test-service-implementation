using App.Codebase.Infrastructure.Services.Analytics;
using App.Codebase.Infrastructure.Services.Analytics.Adapter;
using App.Codebase.Infrastructure.Services.Analytics.Impl;
using VContainer;
using VContainer.Unity;

namespace App.Codebase.Infrastructure.Installer
{
    public class GameLifetimeScope : LifetimeScope
    {
        protected override void Configure(IContainerBuilder builder)
        {
            RegisterServices(builder);
        }

        private void RegisterServices(IContainerBuilder builder)
        {
            builder.Register<IAnalyticsAdapter, FirebaseAnalyticsService>(Lifetime.Singleton);
            builder.Register<IAnalyticsService, AnalyticsService>(Lifetime.Singleton);
        }
    }
}
