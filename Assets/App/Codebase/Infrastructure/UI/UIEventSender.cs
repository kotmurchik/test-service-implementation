using App.Codebase.Infrastructure.Services.Analytics;
using Firebase.Analytics;
using UnityEngine;
using VContainer;

namespace App.Codebase.Infrastructure.UI
{
    public class UIEventSender : MonoBehaviour
    {
        private IAnalyticsService _analyticsService;
        
        [Inject]
        private void Construct(IAnalyticsService analyticsService)
        {
            _analyticsService = analyticsService;
        }

        public void SendTestEvent()
        {
            _analyticsService.TrackEvent(FirebaseAnalytics.EventPostScore, FirebaseAnalytics.ParameterScore, 999);    
        }
    }
}
